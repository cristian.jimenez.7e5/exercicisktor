package ExercicisCorroutines

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withContext
import java.io.BufferedReader
import java.io.File
import java.io.FileReader
import kotlin.system.measureTimeMillis

fun main(){
    val time = measureTimeMillis {
        runBlocking {
            getNextLine()
        }
    }
    println(time)

    val timeInSecs = time/1000
    println("Time in seconds: $timeInSecs")
}

suspend fun getNextLine(){

    val file = File("src/main/resources/songLyrics.txt")

    val bufferedReader = BufferedReader(withContext(Dispatchers.IO) {
        FileReader(file)
    })

    var line: String?

    while (withContext(Dispatchers.IO) {
            bufferedReader.readLine()
        }.also { line = it } != null) {
        println(line)
        delay(3000)
    }
    withContext(Dispatchers.IO) {
        bufferedReader.close()
    }
}