package ExercicisCorroutines

import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking

fun main() {
    runBlocking {
        for(i in 0..10){
            writeHello(i)
        }
    }
    println("Finished!")
}

suspend fun writeHello(i: Int) = coroutineScope {
    launch {
        delay(100)
        println("$i - Hello World")
    }
}