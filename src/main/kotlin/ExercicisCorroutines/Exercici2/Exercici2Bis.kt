package ExercicisCorroutines.Exercici2

import kotlinx.coroutines.*

fun main() {
    GlobalScope.launch  {
        for(i in 0..10){
            writeHello(i)
        }
    }
    println("Finished!")
}

suspend fun writeHello(i: Int){
    delay(100)
    println("$i - Hello World")
}