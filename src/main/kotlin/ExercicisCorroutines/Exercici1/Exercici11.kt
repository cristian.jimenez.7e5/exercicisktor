package ExercicisCorroutines.Exercici1

import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking

fun main() {
    println("The main program is started")
    GlobalScope.launch {
        println("Background processing started")
        delay(1000)
        println("Background processing finished")
    }
    println("The main program continues")
    runBlocking {
        delay(500)
        println("The main program is finished")
    }
}

/*
Ex. 1 ----->
The main program is started
The main program continues
Background processing started
Background processing finished
The main program is finished
 */

/*
Ex. 1.1 ----->
The main program is started
The main program continues
Background processing started
The main program is finished
 */