package ExercicisCorroutines.Exercici1

import kotlinx.coroutines.*

fun main() = runBlocking{
    println("The main program is started")
    doBackground()
    println("The main program continues")
    runBlocking {
        delay(1500)
        println("The main program is finished")
    }
}

suspend fun doBackground() = coroutineScope {
    launch {
        println("Background processing started")
        delay(1000)
        println("Background processing finished")
    }
}