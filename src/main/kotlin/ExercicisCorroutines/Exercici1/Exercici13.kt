package ExercicisCorroutines.Exercici1

import kotlinx.coroutines.*

fun main() = runBlocking{
    println("The main program is started")
    doBackgroundAgain()
    println("The main program continues")
    runBlocking {
        delay(1500)
        println("The main program is finished")
    }
}

suspend fun doBackgroundAgain(){
    withContext(Dispatchers.Default) {
        println("Background processing started")
        delay(1000)
        println("Background processing finished")
    }
}